// ==UserScript==
// @name         No Flash Bullshit
// @namespace    http://tampermonkey.net/
// @version      0.5
// @description  No Flash Bullshit!
// @author       Starfox64
// @downloadURL  https://gitlab.com/Starfox64/no-flash-bullshit/raw/master/script.js
// @match        http*://*/*
// @grant        none
// ==/UserScript==

(function() {
    'use strict';

    setTimeout(function() {
        var bs = document.querySelector('object[type="application/x-shockwave-flash"]');
        if (bs && Number(bs.style['z-index']) >= 10) bs.remove();
    }, 2000);
})();